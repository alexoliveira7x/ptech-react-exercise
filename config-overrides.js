const path = require('path')
const { alias, configPaths } = require('react-app-rewire-alias')
const { override, addWebpackAlias } = require('customize-cra')

module.exports = override(
  alias({
    ...configPaths('tsconfig.base.json'),
    react: path.resolve('./node_modules/react'),
  }),
  addWebpackAlias({
    '@': path.resolve(__dirname, 'src'),
    '@types': path.resolve(__dirname, 'src', 'types'),
  }),
)
