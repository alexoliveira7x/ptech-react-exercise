# Fashion Ecommerce Store

You can follow the following mockups as a reference:

![](Exercise.jpg)

## Technical requirements
The products must be in a grid and each one presents its most relevant image. In addition to the grid mentioned above, there is also another grid that contains those added to Cart. Both should be in different pages.
Both the grid containing the products and the grid containing the Cart can be sorted by price in ascending or descending order.
Also, both grids can be paginated with at most 5 products per page.

When selecting a product, its detail is displayed in a modal. The detail consists of:

Image

Title

Description

Button 'Add to Cart' (if it's already on Cart should say something like 'In Cart' instead).


## Assumptions

Assume that the application is always used by the same user. In other words, there is no need to: login, logout, or maintain separate favorites lists for each user.
However, it should be possible for users to check their previous saved products in Cart each time they go back to the application.


## Rules

Fork this project and submit your work as a merge request.
Use ReactJS or NextJS (Optionally you may use Typescript).
Please contact your Recruiter before coding if you have any doubts about this.
Make use of .gitignore, avoid as much as possible including unnecessary files.
You must use https://fakestoreapi.herokuapp.com/ as an external data source.


## Evaluation criteria

Application meets Technical requirements.
Organization and consistency of the file and folder structure.
Modifiability and extensibility of the system at the required points.
Commit history (commits are organized and descriptive).
Time used to complete the test.
