import styled from 'styled-components'

export const Header = styled.header`
  width: 100%;
  padding: 1.2em 0;
  display: flex;
  background-color: ${props => props.theme.backgroundcolor.primary};
  > div {
    display: flex;
    align-items: center;
  }
`

export const SearchBar = styled.fieldset`
  height: 25px;
  border: none;
  width: 800px;
  background-color: #fff;
  border-radius: ${props => props.theme.grid.borderRadius.nano};
  margin-left: ${props => props.theme.spacing.inline.xxxs};
`
export const SearchInput = styled.input`
  width: 95%;
  height: 100%;
  margin: ${props => props.theme.spacing.squish.nano};
`

export const Main = styled.main`
  display: flex;
  flex: 1;
  flex-direction: column;
`

export const Footer = styled.footer`
  width: 100%;
  height: 72px;
  background-color: ${props => props.theme.backgroundcolor.gray};
  display: flex;
  justify-content: center;
  align-items: center;
  > span {
    color: ${props => props.theme.textcolor.dark};
    font-size: 16px;
  }
`
export const SearchIconBar = styled.i`
  svg {
    max-width: 20px;
    max-height: 16px;
    fill: ${props => props.theme.backgroundcolor.primary};
  }
`

export const ButtonIcon = styled.button`
  width: 25px;
  height: 18px;
`
