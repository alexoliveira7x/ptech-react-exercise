import { CartIcon, CartTotalItems, CartButton } from './style'
import { ReactComponent as CartSvg } from '@/assets/icons/cart.svg'
import { useCartContext } from '@/contexts/Cart'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button'

const Cart: React.FC = () => {
  const { totalItems } = useCartContext()

  return (
    <Button component={Link} to="/Cart">
      <CartButton>
        <CartIcon>
          <CartSvg />
          <CartTotalItems>{totalItems}</CartTotalItems>
        </CartIcon>
      </CartButton>
    </Button>
  )
}

export default Cart
