import styled from 'styled-components'

export const CartButton = styled.button`
  position: relative;
`

export const CartIcon = styled.i`
  fill: #fff;
  margin-left: ${props => props.theme.spacing.inline.xxxs};
  width: 50px;
  svg {
    max-width: 40px;
  }
`
export const CartTotalItems = styled.span`
  width: 12px;
  height: 12px;
  padding: 4px;
  background-color: #fff;
  border-radius: ${props => props.theme.grid.borderRadius.circular};
  font-size: 12px;
  position: absolute;
  top: 2px;
  right: 0;
  text-align: center;
`
