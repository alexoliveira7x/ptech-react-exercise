import {
  Header,
  SearchBar,
  SearchInput,
  SearchIconBar,
  ButtonIcon,
  Footer,
  Main,
} from './style'
import { ReactComponent as Logo } from '@/assets/logo.svg'
import { ReactComponent as SearchIcon } from '@/assets/icons/search.svg'
import Container from '@material-ui/core/Container'

import Cart from './Components/Cart'

const Layout: React.FC = ({ children }) => {
  const reqSearchApp = () => {
    alert('here i need to implement, but its not necessary now :)')
  }

  const handleKeyDown = (Event: KeyboardEvent) => {
    if (Event.key === 'Enter') {
      reqSearchApp()
    }
  }

  return (
    <>
      <Header>
        <Container>
          <Logo />
          <SearchBar>
            <SearchInput
              placeholder="Search by name"
              onKeyDown={handleKeyDown}
            />
            <ButtonIcon>
              <SearchIconBar>
                <SearchIcon />
              </SearchIconBar>
            </ButtonIcon>
          </SearchBar>
          <Cart />
        </Container>
      </Header>
      <Main>{children}</Main>
      <Footer>
        <span>
          Developed with ❤️ by <strong>Alexandro Castro</strong> | Present
          Technologies
        </span>
      </Footer>
    </>
  )
}

export default Layout
