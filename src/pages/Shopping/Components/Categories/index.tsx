import {
  Button,
  List,
  ListItem,
  ListItemText,
  MenuItem,
  Menu,
  Container,
} from '@material-ui/core'
import { useState } from 'react'
import { CategoriesWrapper, CategoriesList, Category } from './style'
import { useFilterContext } from '@/contexts/Filter'

const Categories: React.FC = () => {
  const {
    setCategory,
    category,
    categories,
    setSortByPrice,
  } = useFilterContext()
  const sort = ['relevance', 'lower', 'higher']

  const handleSetCategory = (category: string) => {
    setCategory(category.toLowerCase())
  }

  const [anchorEl, setAnchorEl] = useState(null)
  const [selectedIndex, setSelectedIndex] = useState(0)

  const handleClickListItem = event => {
    setAnchorEl(event.currentTarget)
  }

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index)
    setAnchorEl(null)
    setSortByPrice(sort[index])
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  const options = ['Relevance', 'Price: Low to High', 'Price: High to Low']

  return (
    <CategoriesWrapper>
      <Container>
        <CategoriesList>
          <Category>
            <Button
              color={category === 'all' ? 'secondary' : 'default'}
              onClick={() => handleSetCategory('all')}
            >
              All categories
            </Button>
          </Category>
          {categories.map((categoryName, key) => {
            return (
              <Category key={key}>
                <Button
                  color={categoryName === category ? 'secondary' : 'default'}
                  onClick={() => handleSetCategory(categoryName)}
                >
                  {categoryName}
                </Button>
              </Category>
            )
          })}
        </CategoriesList>
      </Container>
      <Container>
        <List component="nav">
          <ListItem button onClick={handleClickListItem}>
            <ListItemText
              primary="Sorting by"
              secondary={options[selectedIndex]}
            />
          </ListItem>
        </List>
        <Menu
          id="lock-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          {options.map((option, index) => (
            <MenuItem
              key={option}
              selected={index === selectedIndex}
              onClick={event => handleMenuItemClick(event, index)}
            >
              {option}
            </MenuItem>
          ))}
        </Menu>
      </Container>
    </CategoriesWrapper>
  )
}

export default Categories
