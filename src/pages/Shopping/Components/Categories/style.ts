import styled from 'styled-components'

export const CategoriesWrapper = styled.div`
  width: 100%;
  background-color: ${props => props.theme.backgroundcolor.gray};
  margin-bottom: ${props => props.theme.spacing.stack.xs};
`

export const CategoriesList = styled.ul`
  display: flex;
  height: 40px;
  align-items: center;
`
export const Category = styled.li`
  font-size: 16px;
  cursor: pointer;
  padding: ${props => props.theme.spacing.squish.xxxs};
`
