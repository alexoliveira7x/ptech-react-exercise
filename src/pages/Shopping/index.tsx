import Categories from './Components/Categories'
import Products from './Products'

const Shopping: React.FC = () => {
  return (
    <div className="pageContainer">
      <Categories />
      <Products />
    </div>
  )
}

export default Shopping
