import { useEffect, useState, useCallback, ChangeEvent } from 'react'

import {
  Container,
  Button,
  Card,
  CardHeader,
  CardMedia,
  CardActions,
} from '@material-ui/core'
import { Skeleton, Pagination } from '@material-ui/lab'
import { useStyles, ProductList, CardTitle } from './style'
import { PaginationWrapper } from '@/styles/shared'
import { AxiosResponse } from 'axios'
import { IPaginate } from '@/types'
import { useCartContext } from '@/contexts/Cart'
import { useFilterContext } from '@/contexts/Filter'
import ProductDetail from './Components/ProductDetail'
import Api from '@/services/api'
import Paginator from '@/utils/pagination'

const fetchProducts = async () => {
  return await Api.get('products').then(
    (response: AxiosResponse) => response.data,
  )
}

const fetchProductsCategory = async (category: string) => {
  return await Api.get(`products/category/${category}`).then(
    (response: AxiosResponse) => response.data,
  )
}

const Products: React.FC = () => {
  const { category, sortByPrice } = useFilterContext()
  const { addItem, hasItemInCart, updateItem } = useCartContext()

  const [products, SetProducts] = useState<IPaginate>({
    previousPage: 1,
    nextPage: 1,
    total: 1,
    totalPages: 1,
    items: [],
  })

  const [page, setPage] = useState<number>(1)
  const [loading, setLoading] = useState<boolean>(false)

  const setPagination = (products, page: number) => {
    const data = Paginator({ items: products, filter: sortByPrice, page })
    SetProducts(data)
  }

  const reqProducts = (page: number) => {
    setLoading(true)

    fetchProducts().then(products => {
      setPagination(products, page)
      setLoading(false)
    })
  }
  const reqCategoryProducts = (category: string) => {
    setLoading(true)

    fetchProductsCategory(category).then(products => {
      setPagination(products, page)
      setLoading(false)
    })
  }

  const handleChangePage = useCallback(
    async (event: ChangeEvent, currentPage: number) => {
      if (page === currentPage) return
      setPage(currentPage)
    },
    [page],
  )

  useEffect(() => {
    category === 'all' ? reqProducts(page) : reqCategoryProducts(category)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, category, sortByPrice])

  const styleMui = useStyles()
  const handleClickCart = useCallback(product => {
    addItem(product)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleUpdateCart = useCallback(product => {
    updateItem(product)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Container>
      <ProductList>
        {loading
          ? [...Array(5)].map((e, i) => {
              return (
                <Skeleton key={i} variant="rect" width={250} height={300} />
              )
            })
          : products.items.map(product => {
              return (
                <Card key={product.id}>
                  <CardTitle>
                    <CardHeader
                      title={product.title}
                      subheader={product.category}
                    />
                  </CardTitle>
                  <CardMedia
                    image={product.image}
                    className={styleMui.media}
                    title={product.title}
                  />
                  <CardActions>Price: $ {product.price}</CardActions>
                  <CardActions className={styleMui.actions}>
                    <ProductDetail product={product} />
                    {hasItemInCart(product.id) ? (
                      <>
                        <Button
                          onClick={() => handleUpdateCart(product)}
                          color="primary"
                        >
                          Add to Cart (1)
                        </Button>
                      </>
                    ) : (
                      <Button
                        onClick={() => handleClickCart(product)}
                        color="secondary"
                      >
                        Add to cart
                      </Button>
                    )}
                  </CardActions>
                </Card>
              )
            })}
      </ProductList>
      <PaginationWrapper>
        <Pagination
          count={products.totalPages}
          page={page}
          onChange={handleChangePage}
          color="secondary"
        />
      </PaginationWrapper>
    </Container>
  )
}

export default Products
