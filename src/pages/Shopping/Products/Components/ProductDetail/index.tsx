import { useState } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import CardMedia from '@material-ui/core/CardMedia'
import { useStyles } from '../../style'
import { useCartContext } from '@/contexts/Cart'
import { ProductDetailProps } from '@/types'

const ProductDetail: React.FC<ProductDetailProps> = ({ product }) => {
  const { addItem, hasItemInCart } = useCartContext()
  const { title, price, category, description, image, id } = product
  const [open, setOpen] = useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleClickCart = () => {
    addItem({
      ...product,
      quantity: 1,
    })
  }

  const styleMui = useStyles()

  return (
    <>
      <Button onClick={handleClickOpen} color="primary">
        Detail
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>Category: {category}</DialogContentText>
          <DialogContentText>{description}</DialogContentText>
          <CardMedia image={image} className={styleMui.mediaFS} title={title} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          {hasItemInCart(id) ? (
            <Button disabled color="primary">
              In Cart
            </Button>
          ) : (
            <Button onClick={() => handleClickCart()} color="secondary">
              Add to cart ${price}
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </>
  )
}

export default ProductDetail
