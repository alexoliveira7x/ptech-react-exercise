import { makeStyles } from '@material-ui/core/styles'
import styled from 'styled-components'

export const useStyles = makeStyles(() => ({
  actions: {
    display: 'flex',
    height: '40px',
  },
  media: {
    height: 200,
    width: 200,
    backgroundSize: 'contain',
  },
  mediaFS: {
    height: 400,
    backgroundSize: 'contain',
    display: 'flex',
    justifyContent: 'center',
  },
}))

export const ProductList = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 250px);
  grid-template-rows: 400px;
  grid-gap: 16px;
  > div {
    max-width: 250px;
  }
`

export const CardTitle = styled.div`
  span:first-child {
    font-size: 16px;
    text-overflow: ellipsis;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    height: 45px;
  }
`
