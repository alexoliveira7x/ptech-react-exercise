import styled from 'styled-components'

export const CartWrapper = styled.div`
  margin-top: ${props => props.theme.spacing.stack.xs};
  .MuiButton-label {
    display: flex;
    align-items: center;
  }
`

export const ButtonIcon = styled.i`
  margin-right: ${props => props.theme.spacing.inline.xxxs};
  display: flex;
  svg {
    fill: ${props => props.theme.backgroundcolor.primary};
  }
`

export const ButtonBack = {
  fontSize: '15px',
}
