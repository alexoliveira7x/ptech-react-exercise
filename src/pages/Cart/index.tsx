import { useState, useEffect, useCallback, ChangeEvent } from 'react'
import {
  List,
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography,
  Container,
} from '@material-ui/core'
import { CartWrapper, ButtonIcon, ButtonBack } from './style'
import { ReactComponent as BackIcon } from '@/assets/icons/left-arrow.svg'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import { useCartContext } from '@/contexts/Cart'
import Paginator from '@/utils/pagination'
import { useFilterContext } from '@/contexts/Filter'
import { Pagination } from '@material-ui/lab'
import { PaginationWrapper } from '@/styles/shared'
import { IPaginate } from '@/types'

const Cart: React.FC = () => {
  const { cart } = useCartContext()
  const { sortByPrice } = useFilterContext()

  const [page, setPage] = useState<number>(1)
  const [paginatedCart, setPaginateCart] = useState<IPaginate>({
    previousPage: 1,
    nextPage: 1,
    total: 1,
    totalPages: 1,
    items: [],
  })

  const handleChangePage = useCallback(
    async (event: ChangeEvent, currentPage: number) => {
      if (page === currentPage) return
      setPage(currentPage)
    },
    [page],
  )

  useEffect(() => {
    const data = Paginator({ items: cart, filter: sortByPrice, page })
    setPaginateCart(data)
  }, [cart, page, sortByPrice])

  return (
    <CartWrapper>
      <Container>
        <Button component={Link} to="/" color="secondary" style={ButtonBack}>
          <ButtonIcon>
            <BackIcon />
          </ButtonIcon>
          Continue Shopping
        </Button>
      </Container>
      <Container>
        {paginatedCart.items.map(productCart => {
          return (
            <List key={productCart.id}>
              <ListItem alignItems="flex-start">
                <ListItemAvatar>
                  <Avatar alt={productCart.title} src={productCart.image} />
                </ListItemAvatar>
                <ListItemText
                  primary={productCart.title}
                  secondary={
                    <>
                      <Typography
                        component="span"
                        variant="body2"
                        color="textPrimary"
                      >
                        Category: {productCart.category}
                      </Typography>
                      <Typography
                        component="p"
                        variant="body2"
                        color="textPrimary"
                      >
                        Price: $ {productCart.price}
                      </Typography>
                    </>
                  }
                />
              </ListItem>
              <Divider variant="inset" component="li" />
            </List>
          )
        })}
      </Container>
      <Container>
        <PaginationWrapper>
          <Pagination
            count={paginatedCart.totalPages}
            page={page}
            onChange={handleChangePage}
            color="secondary"
          />
        </PaginationWrapper>
      </Container>
    </CartWrapper>
  )
}

export default Cart
