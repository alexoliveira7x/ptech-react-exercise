import { IPaginate } from '@/types'

// eslint-disable-next-line
const sortByPriceAsc = items => {
  return items.sort((a, b) => a.price - b.price)
}

// eslint-disable-next-line
const sortByPriceDesc = items => {
  return items.sort((a, b) => b.price - a.price)
}

const paginate = ({
  items,
  filter,
  page = 1,
  perPage = 5,
}: {
  items: any // eslint-disable-line
  filter: string
  page?: number
  perPage?: number
}): IPaginate => {
  try {
    switch (filter) {
      case 'higher':
        items = sortByPriceDesc(items)
        break
      case 'lower':
        items = sortByPriceAsc(items)
        break
    }

    const offset = perPage * (page - 1)
    const totalPages = Math.ceil(items.length / perPage)
    const paginatedItems = items.slice(offset, perPage * page)

    return {
      previousPage: page - 1 ? page - 1 : null,
      nextPage: totalPages > page ? page + 1 : null,
      total: items.length,
      totalPages: totalPages,
      items: paginatedItems,
    }
  } catch (e) {
    return {
      previousPage: null,
      nextPage: null,
      total: 1,
      totalPages: 1,
      items: [],
    }
  }
}

export default paginate
