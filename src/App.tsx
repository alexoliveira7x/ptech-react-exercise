import useHeight from '@/utils/useHeight' // fix Safari problem with 100vh
import Router from '@/router'
import '@/styles/global.css'

const App: React.FC = () => {
  const height = useHeight()

  return (
    <div className="mainContainer" style={{ minHeight: height }}>
      <Router />
    </div>
  )
}

export default App
