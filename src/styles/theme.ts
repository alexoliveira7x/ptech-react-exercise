export const theme = {
  backgroundcolor: {
    primary: '#E60014',
    gray: '#eaeaea',
  },
  textcolor: {
    primary: '#E60014',
    dark: '#666',
  },
  spacing: {
    inline: {
      // quarck: '4px',
      // nano: '8px',
      xxxs: '16px',
    },
    squish: {
      nano: '2px 4px',
      xxxs: '4px 8px',
    },
    stack: {
      xs: '32px',
      sm: '48px',
      md: '56px',
    },
  },
  grid: {
    borderRadius: {
      nano: '4px',
      circular: '50%',
    },
  },
}
