import styled from 'styled-components'

export const PaginationWrapper = styled.div`
  margin-top: ${props => props.theme.spacing.stack.md};
  display: flex;
  justify-content: center;
`
