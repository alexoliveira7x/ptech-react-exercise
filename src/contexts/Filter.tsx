import { createContext, useState, useContext, useEffect } from 'react'
import { AxiosResponse } from 'axios'
import Api from '@/services/api'

interface IFilterContext {
  category: string
  categories: string[]
  search: string
  sortByPrice: string
  setCategory: React.Dispatch<React.SetStateAction<string>>
  setSearch: React.Dispatch<React.SetStateAction<string>>
  setSortByPrice: React.Dispatch<React.SetStateAction<string>>
}

const FilterContext = createContext<IFilterContext>(null)

export const FilterProvider: React.FC = ({ children }) => {
  const [category, setCategory] = useState<string>('all')
  const [categories, setCategories] = useState<string[]>([])
  const [search, setSearch] = useState<string>('')
  const [sortByPrice, setSortByPrice] = useState<string>('relevance')

  const fetchCategories = async () => {
    return await Api.get('products/categories').then(
      (response: AxiosResponse) => response.data,
    )
  }

  useEffect(() => {
    fetchCategories().then(categories => {
      setCategories(categories)
    })
  }, [])

  return (
    <FilterContext.Provider
      value={{
        category,
        search,
        sortByPrice,
        categories,
        setCategory,
        setSearch,
        setSortByPrice,
      }}
    >
      {children}
    </FilterContext.Provider>
  )
}

export const useFilterContext = (): IFilterContext => {
  const context = useContext(FilterContext)
  if (!context) {
    throw new Error('useFilterContext must be used within a FilterProvider')
  }
  return context
}
