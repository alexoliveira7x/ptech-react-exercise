import {
  createContext,
  useState,
  useContext,
  useCallback,
  useEffect,
} from 'react'
import { ICart, ICartContext } from '@/types'

const getCartCache = () => {
  const localCart = localStorage.getItem('@preset/cart')
  try {
    return JSON.parse(localCart)
  } catch (e) {
    return []
  }
}

const CartContext = createContext<ICartContext>(null)

export const CartProvider: React.FC = ({ children }) => {
  const [cart, setCartItems] = useState<ICart[]>(null)
  const [totalItems, setTotalItems] = useState<number>(0)

  useEffect(() => {
    setCartItems(getCartCache())
  }, [])

  const getTotalItems = (array): number => {
    array = array ?? []
    return array.reduce((prev, next) => prev + next.quantity, 0)
  }

  useEffect(() => {
    setTotalItems(getTotalItems(cart))
  }, [cart])

  const hasItemInCart = (id: number) => {
    return (
      Object.keys((cart || []).find(product => product.id === id) || [])
        .length > 0
    )
  }

  const storeCartCache = (cart: ICart[]) => {
    localStorage.setItem('@preset/cart', JSON.stringify(cart))
  }

  const addItem = useCallback((item: ICart) => {
    setCartItems(oldCartItems => {
      const data = [
        ...(oldCartItems || []),
        {
          ...item,
          quantity: 1,
        },
      ]
      storeCartCache(data)
      return data
    })
  }, [])

  const updateItem = useCallback((item: ICart) => {
    setCartItems(oldCartItems => {
      const i = oldCartItems.findIndex(product => product.id === item.id)
      oldCartItems[i].quantity += 1
      storeCartCache(oldCartItems)
      setTotalItems(getTotalItems(oldCartItems))
      return oldCartItems
    })
  }, [])

  const getTotalItemsById = (id: number): number => {
    return ((cart || []).find(product => product.id === id) || {}).quantity || 0
  }

  return (
    <CartContext.Provider
      value={{
        cart,
        addItem,
        hasItemInCart,
        updateItem,
        totalItems,
        getTotalItemsById,
      }}
    >
      {children}
    </CartContext.Provider>
  )
}

export const useCartContext = (): ICartContext => {
  const context = useContext(CartContext)
  if (!context) {
    throw new Error('useCartContext must be used within a CartProvider')
  }
  return context
}
