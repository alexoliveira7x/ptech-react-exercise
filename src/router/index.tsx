import { Route, Switch, Redirect } from 'react-router-dom'
import { Layout, Shopping, Cart } from '@/pages'
import { ThemeProvider } from 'styled-components'
import { theme } from '../styles/theme'
import { CartProvider } from '@/contexts/Cart'
import { FilterProvider } from '@/contexts/Filter'

const Router: React.FC = props => {
  return (
    <ThemeProvider theme={theme}>
      <CartProvider>
        <FilterProvider>
          <Layout>
            <Switch {...props}>
              <Route exact path="/" component={Shopping} />
              <Route exact path="/cart" component={Cart} />
              <Redirect to="/" />
            </Switch>
          </Layout>
        </FilterProvider>
      </CartProvider>
    </ThemeProvider>
  )
}

export default Router
