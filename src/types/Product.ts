export interface IProduct {
  id: number
  title: string
  price: number
  description?: string
  image: string
  category?: string
}

export interface IProducts {
  previousPage: number
  nextPage: number
  total: number
  totalPages: number
  items: IProduct[]
}

export interface ProductDetailProps {
  product: IProduct
}
