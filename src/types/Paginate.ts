export interface IPaginate {
  previousPage?: number
  nextPage?: number
  total?: number
  totalPages?: number
  items: any[] // eslint-disable-line
}
