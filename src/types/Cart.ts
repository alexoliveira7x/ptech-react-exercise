export interface ICart {
  id: number
  title: string
  price: number
  description?: string
  image: string
  category?: string
  quantity: number
}

export interface ICartContext {
  cart: Array<ICart>
  totalItems: number
  addItem: (item: ICart) => void
  updateItem: (item: ICart) => void
  hasItemInCart: (id: number) => boolean
  getTotalItemsById: (id: number) => number
}
